FROM ubuntu:22.04 as builder

ARG BUILD_PREFIX="/usr/local"
ARG CC65_PREFIX="/usr/local/cc65"
ARG LOADER="loader-v194.zip"

### Install CC65 binaries

COPY --from=registry.gitlab.com/robotriot/cc65:2.19-2 "${CC65_PREFIX}" "${CC65_PREFIX}"
ENV PATH="${CC65_PREFIX}/bin:${PATH}"

### Install apt packages

RUN set -xe \
    && apt-get update \
    && apt-get install -y --no-install-recommends build-essential ca-certificates rustc cargo perl python-is-python3 zip unzip acme golang git bsdextrautils

### Check all required binaries' versions

RUN set -xe \
    && cc65 --version \
    && ca65 --version \
    && ld65 --version \
    && gcc --version \
    && cpp --version \
    && cc --version \
    && make --version \
    && rustc --version \
    && cargo --version \
    && perl --version \
    && python --version \
    && acme --version \
    && go version \
    && git --version

### Prepare loader source

COPY "${LOADER}" /src/
RUN set -xe \
    && cd  /src \
    && unzip "${LOADER}" \
    && rm "${LOADER}"

WORKDIR /src/loader

### Build loader and all tools

RUN set -xe \
    && cd "$(go env GOROOT)/src/" \
    && git clone -b 'v1.1.0' --single-branch https://github.com/RyanCarrier/dijkstra.git \
    && cd /src/loader/tools/tscrunch \
    && go build tscrunch.go

RUN set -xe \
    && PREFIX="${BUILD_PREFIX}" make tools


FROM ubuntu:22.04

ARG CC65_PREFIX="/usr/local/cc65"
ARG BUILD_PREFIX="/usr/local"
ENV BUILD_PREFIX="${BUILD_PREFIX}"

COPY --from=registry.gitlab.com/robotriot/cc65:2.19-2 "${CC65_PREFIX}" "${CC65_PREFIX}"
ENV PATH="${CC65_PREFIX}/bin:${PATH}"

COPY --from=builder /src/ /src/

WORKDIR /src/loader/

RUN set -xe \
    && rm -rf build \
    && mkdir -p /build/ \
    && ln -s /build build


RUN set -xe \
    && apt-get update \
    && apt-get install -y --no-install-recommends perl python-is-python3 make zip \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Krill's Loader unofficial Docker image

Based on Ubuntu Linux LTS. Linux CLI knowledge required. Docker knowledge
recommended.

## Contents
- Krill's Loader (https://csdb.dk/search/?search=Krill+Loader)
- prerequisites (from Ubuntu): perl, python, make, zip
- cca65 (https://github.com/cc65/cc65)
- all tools in the `tools/` directory have been compiled (so you can use them to pack stuff)

## How to build a loader
To build your loader, mount the build output directory as `/build/` into the
container and execute `make prg` with the desired parameters.
```shell
docker run --volume /your/loader/build/dir:/build/ registry.gitlab.com/robotriot/krills-loader:194-1 make prg
```

E.g. to build a loader with the installer at $1000, the resident part at $8000
using zeropage addresses starting from $80 into a directory `loader-build`
inside your current directory, use:
```shell
docker run --volume "$(pwd)/loader-build":/build/ registry.gitlab.com/robotriot/krills-loader:194-1 make prg ZP=80 INSTALL=1000 RESIDENT=8000
```

## How to use a cruncher/tool
As all supported cruncher binaries in the `tools/` directory have been
compiled, you can use them in a similar manner as the loader builder
itself:
```shell
docker run --volume "$(pwd)/packed":/packed/ registry.gitlab.com/robotriot/krills-loader:194-1 <tool command here>
```
This command creates (or uses) a local directory `packed` (inside your current
directory) and mounts it inside the container as `/packed/`. You need to put
your files to be used by the tools inside your local `packed` directory - this
way, the files will appear in the container in the `/packed/` directory where
you can then let the tools read them from (and write them back to).

### B2
`tools/b2/b2.exe` (don't let the extension fool you, it's a Linux executable ;))
### Bitnax
`tools/bitnax-07a8c67/lz`
### CC1541
`tools/cc1541/cc1541`
### Dali
`tools/dali/dali`
### Doynamite
`tools/doynamite1.1/lz`
### Exomizer
`tools/exomizer-3.1/src/exobasic` and
`tools/exomizer-3.1/src/exomizer`
### LevelCrush converter
`tools/compressedfileconverter.pl`
### LZSA
`tools/lzsa/lzsa`
### NuCrunch
`tools/nucrunch-1.0.1/target/release/nucrunch`
with the C64 binaries being in
`tools/nucrunch-1.0.1/bin/`
### PuCrunch
`tools/pucrunch/pucrunch`
### Subsizer
`tools/subsizer-0.7pre1/subsizer`
### TinyCrunch
This one is being invoked by the `make` command. Consult the TC documentation
for usage details. TinyCrunches `make` command can be invoked like this:
```shell
docker run --volume "$(pwd)/packed":/packed/ registry.gitlab.com/robotriot/krills-loader:194-1 make -C tools/tinycrunch_v1.2 <tinycrunch command here>
```
### TSCrunch
`tools/tscrunch/tscrunch`
### WCrush
`tools/wcrush/wca/wca` and `tools/wcrush/wcrush/wcrush`
